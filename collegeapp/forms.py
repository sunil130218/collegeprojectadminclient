from django import forms
from .models import *


class ContactForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['name', 'email', 'subject', 'message', ]
        widgets = {
            'email': forms.EmailInput(attrs={
            })
        }


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['email']
        widgets = {
            'email': forms.EmailInput(attrs={
                'type': 'email',
                'class': 'text',
                'placeholder': 'enter mail here',
            })
        }
