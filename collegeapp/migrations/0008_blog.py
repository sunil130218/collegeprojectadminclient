# Generated by Django 2.0.7 on 2018-08-08 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collegeapp', '0007_gallery'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('detail', models.TextField(max_length=200)),
                ('author', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='blogs')),
                ('date', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
