# Generated by Django 2.0.7 on 2018-08-08 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collegeapp', '0008_blog'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Blog',
        ),
        migrations.RemoveField(
            model_name='news',
            name='heading',
        ),
        migrations.AddField(
            model_name='news',
            name='author',
            field=models.CharField(default='sunil', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='news',
            name='title',
            field=models.CharField(default='uuuu', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='news',
            name='detail',
            field=models.TextField(max_length=200),
        ),
        migrations.AlterField(
            model_name='news',
            name='image',
            field=models.ImageField(upload_to='blogs'),
        ),
    ]
