from django.db import models


class Message(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    subject = models.CharField(max_length=100)
    message = models.TextField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Slider(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='sliders')

    def __str__(self):
        return self.title


class Staff(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='staffs')
    detail = models.TextField(max_length=500)
    facebook = models.CharField(max_length=50)
    twitter = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Service(models.Model):
    title = models.CharField(max_length=200)
    detail = models.TextField(max_length=500)
    icon = models.CharField(max_length=200)
    image = models.ImageField(upload_to='services')

    def __str__(self):
        return self.title


class Subscriber(models.Model):
    email = models.EmailField()

    def __str__(self):
        return self.email


class Gallery(models.Model):
    image = models.ImageField(upload_to='galleries')
    caption = models.CharField(max_length=50)

    def __str__(self):
        return self.image.url


class News(models.Model):
    title = models.CharField(max_length=50)
    detail = models.TextField()
    author = models.CharField(max_length=50)
    image = models.ImageField(upload_to='blogs')
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
