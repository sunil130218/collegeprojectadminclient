from django.views.generic import *
from .models import *
from .forms import *


class ClientMixin(object):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sliders'] = Slider.objects.all()
        context['staffs'] = Staff.objects.all()
        context['services'] = Service.objects.all()
        context['news'] = News.objects.all()
        context['subscriberform'] = SubscriberForm
        return context


class ClientHomeView(ClientMixin, TemplateView):
    template_name = 'clienttemplates/clienthome.html'


class ClientContactView(ClientMixin, CreateView):
    template_name = 'clienttemplates/clientcontact.html'
    form_class = ContactForm
    success_url = '/'


class ClientAboutView(ClientMixin, TemplateView):
    template_name = 'clienttemplates/clientabout.html'


class ClientServiceDetailView(ClientMixin, DetailView):
    template_name = 'clienttemplates/clientservicedetail.html'
    model = Service
    context_object_name = 'service'


class ClientNewsListView(ClientMixin, ListView):
    template_name = 'clienttemplates/clientnewslist.html'
    model = News
    context_object_name = 'newslist'


class ClientNewsDetailView(ClientMixin, DetailView):
    template_name = 'clienttemplates/clientnewsdetail.html'
    model = News
    context_object_name = 'newsdetail'


class ClientSubscriberView(ClientMixin, CreateView):
    template_name = 'clienttemplates/clientsubsciber.html'
    form_class = SubscriberForm
    success_url = '/'


class ClientGalleryListView(ClientMixin, ListView):
    template_name = 'clienttemplates/clientgallerylist.html'
    model = Gallery
    context_object_name = 'gallerylist'

