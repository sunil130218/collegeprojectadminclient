from django.urls import path
from .views import *


app_name = 'collegeapp'
urlpatterns = [
    path('', ClientHomeView.as_view(), name='clienthome'),
    path('contact/', ClientContactView.as_view(), name='clientcontact'),
    path('about/', ClientAboutView.as_view(), name='clientabout'),
    path('gallery/', ClientGalleryListView.as_view(),
         name='clientgallerylist'),
    path('services/<int:pk>/', ClientServiceDetailView.as_view(),
         name='clientservicedetail'),
    path('news/', ClientNewsListView.as_view(), name='clientnewslist'),
    path('news/<int:pk>/', ClientNewsDetailView.as_view(),
         name='clientnewsdetail'),
    path('subscriber/', ClientSubscriberView.as_view(),
         name='clientsubscriber'),
]
<html>  
<title>

</html>